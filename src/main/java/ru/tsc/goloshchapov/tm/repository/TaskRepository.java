package ru.tsc.goloshchapov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.goloshchapov.tm.api.repository.ITaskRepository;
import ru.tsc.goloshchapov.tm.enumerated.Status;
import ru.tsc.goloshchapov.tm.model.Task;

import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public final class TaskRepository extends AbstractOwnerRepository<Task> implements ITaskRepository {

    @Nullable
    @Override
    public Task findByName(@NotNull final String userId, @NotNull final String name) {
        return entities.stream()
                .filter(u -> userId.equals(u.getUserId()))
                .filter(e -> name.equals(e.getName()))
                .findFirst()
                .orElse(null);
    }

    @Nullable
    @Override
    public Task removeByName(@NotNull final String userId, @NotNull final String name) {
        @NotNull final Optional<Task> task = Optional.ofNullable(findByName(userId, name));
        task.ifPresent(this::remove);
        return task.orElse(null);
    }

    @Nullable
    @Override
    public Task startById(@NotNull final String userId, @NotNull final String id) {
        @NotNull final Optional<Task> task = Optional.ofNullable(findById(userId, id));
        task.ifPresent(t -> {
            t.setStatus(Status.IN_PROGRESS);
            t.setStartDate(new Date());
        });
        return task.orElse(null);
    }

    @Nullable
    @Override
    public Task startByIndex(@NotNull final String userId, @NotNull final Integer index) {
        @NotNull final Optional<Task> task = Optional.ofNullable(findByIndex(userId, index));
        task.ifPresent(t -> {
            t.setStatus(Status.IN_PROGRESS);
            t.setStartDate(new Date());
        });
        return task.orElse(null);
    }

    @Nullable
    @Override
    public Task startByName(@NotNull final String userId, @NotNull final String name) {
        @NotNull final Optional<Task> task = Optional.ofNullable(findByName(userId, name));
        task.ifPresent(t -> {
            t.setStatus(Status.IN_PROGRESS);
            t.setStartDate(new Date());
        });
        return task.orElse(null);
    }

    @Nullable
    @Override
    public Task finishById(@NotNull final String userId, @NotNull final String id) {
        @NotNull final Optional<Task> task = Optional.ofNullable(findById(userId, id));
        task.ifPresent(t -> {
            t.setStatus(Status.COMPLETED);
            t.setFinishDate(new Date());
        });
        return task.orElse(null);
    }

    @Nullable
    @Override
    public Task finishByIndex(@NotNull final String userId, @NotNull final Integer index) {
        @NotNull final Optional<Task> task = Optional.ofNullable(findByIndex(userId, index));
        task.ifPresent(t -> {
            t.setStatus(Status.COMPLETED);
            t.setFinishDate(new Date());
        });
        return task.orElse(null);
    }

    @Nullable
    @Override
    public Task finishByName(@NotNull final String userId, @NotNull final String name) {
        @NotNull final Optional<Task> task = Optional.ofNullable(findByName(userId, name));
        task.ifPresent(t -> {
            t.setStatus(Status.COMPLETED);
            t.setFinishDate(new Date());
        });
        return task.orElse(null);
    }

    @Nullable
    @Override
    public Task changeStatusById(@NotNull final String userId, @NotNull final String id, @NotNull final Status status) {
        @NotNull final Optional<Task> task = Optional.ofNullable(findById(userId, id));
        task.ifPresent(t -> {
            t.setStatus(status);
            if (status == Status.IN_PROGRESS) t.setStartDate(new Date());
            if (status == Status.COMPLETED) t.setFinishDate(new Date());
        });
        return task.orElse(null);
    }

    @Nullable
    @Override
    public Task changeStatusByIndex(@NotNull final String userId, @NotNull final Integer index, @NotNull final Status status) {
        @NotNull final Optional<Task> task = Optional.ofNullable(findByIndex(userId, index));
        task.ifPresent(t -> {
            t.setStatus(status);
            if (status == Status.IN_PROGRESS) t.setStartDate(new Date());
            if (status == Status.COMPLETED) t.setFinishDate(new Date());
        });
        return task.orElse(null);
    }

    @Nullable
    @Override
    public Task changeStatusByName(@NotNull final String userId, @NotNull final String name, @NotNull final Status status) {
        @NotNull final Optional<Task> task = Optional.ofNullable(findByName(userId, name));
        task.ifPresent(t -> {
            t.setStatus(status);
            if (status == Status.IN_PROGRESS) t.setStartDate(new Date());
            if (status == Status.COMPLETED) t.setFinishDate(new Date());
        });
        return task.orElse(null);
    }

    @NotNull
    @Override
    public List<Task> findAllTaskByProjectId(@NotNull final String userId, @NotNull final String projectId) {
        return entities.stream()
                .filter(u -> userId.equals(u.getUserId()))
                .filter(t -> projectId.equals(t.getProjectId()))
                .collect(Collectors.toList());
    }

    @Override
    public void removeAllTaskByProjectId(@NotNull final String userId, @NotNull final String projectId) {
        @NotNull Optional<List<Task>> listByProject = Optional.of(findAllTaskByProjectId(userId, projectId));
        listByProject.ifPresent(entities::removeAll);
    }

    @Nullable
    @Override
    public Task bindTaskToProjectById(@NotNull final String userId, @NotNull final String projectId, @NotNull final String taskId) {
        @NotNull final Optional<Task> task = Optional.ofNullable(findById(userId, taskId));
        task.ifPresent(t -> t.setProjectId(projectId));
        return task.orElse(null);
    }

    @Nullable
    @Override
    public Task unbindTaskById(@NotNull final String userId, @NotNull final String taskId) {
        @NotNull final Optional<Task> task = Optional.ofNullable(findById(userId, taskId));
        task.ifPresent(t -> t.setProjectId(null));
        return task.orElse(null);
    }

}
