package ru.tsc.goloshchapov.tm.api.entity;

import ru.tsc.goloshchapov.tm.enumerated.Status;

public interface IHasStatus {

    Status getStatus();

    void setStatus(Status status);

}
